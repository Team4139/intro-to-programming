### INTRO-TO-PROGRAMMING ###

## A collection of files used for reference and practice for the programming team ##

## Features: ##

- UNIX/Linux Quick Reference

- Git Quick Reference

- Short project to practice Git and Unix

- Contributions Guidelines Quick Reference


## FRC Java References: ##

- [Creating and running Java Programs](http://wpilib.screenstepslive.com/s/4485/m/13809/c/57246)
- [2017 FRC Java API](http://first.wpi.edu/FRC/roborio/release/docs/java/)