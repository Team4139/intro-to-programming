# This is a basic introduction to using a Git repository

### IMPORTANT TIPS ###

- Always run the git config commands before working on a new computer
- Always pull before you push
- Keep commit message headers short and simple
- Always make your changes in a different branch, then merge them into master

### BASIC COMMANDS ###
To clone a repository into the current directory:
`git clone </url/to/git>`

To setup your name and email in the repository:
`git config --global user.name <username>`

`git config --global user.email <email_address>`

To download changes from the server:
`git pull`

To view a changelog diagram:
`git log --graph`

### CREATING CHANGES ###
To stage a file to commit:
`git add </path/to/file>`

To stage all files in the current directory:
`git add .`

To commit all staged changes:
`git commit -m '<commit_message>'`

To send a commit to the server:
`git push origin <branch_name>`

To merge a branch into the current branch:
`git merge <other_branch_name>`

### NAVIGATING THE REPO ###
To create a new branch:
`git branch <branch_name>`

To delete a branch (You cannot delete a branch you are in):
`git branch -d <branch_name>`

To switch to a different branch:
`git checkout <branch_name>`

To list all current branches:
`git branch`
