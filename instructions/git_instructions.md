# Instructions for an introductory project to Git

# During this project, you will practice the basics of using a git repository #

### YOU MAY REFER TO THE UNIX.MD AND GIT.MD INSTRUCTIONS PAGES AT ANY TIME ###

### INSTRUCTIONS ###

Start by creating a directory named 'repositories' inside your home directory

Configure your username and email

Inside it, clone the git repository at <https://gitlab.com/Team4139/intro-to-programming.git>

Change directory into newly cloned repository

Create a new git branch named `yourname`

Switch into new branch

Go to the `tmp` directory and create and edit a new file, named `yourname.md`

Inside it, type the name of your favorite type of dog

Stage that file, then git commit it with the message `Yourname Intro Commit`

Git merge it into master and then switch back to master branch

Git push changes from master

Once you have finished, let Frank or Jack know.
