# This is a basic introduction to bash/unix commandline use and navigation
# Sections are headed with triple-#'s

### IMPORTANT TIPS ###

- A directory is the unix name for a folder
- You do not need to include the full path of something if it is inside the current directory
- You should NEVER have a filename or a directory name that includes a space
- You can use the up and down arrow keys to move through your command history

### BASIC COMMANDS ###
To cancel the current command:
`ctrl+c`

To clear the console:
`ctrl+l`

To autocomplete the current word (works in most cases):
`*tab*`

To get help information on a command (works in most cases):
`<command> --help` or `man <command>`

To print the contents of a file to the console:
`cat </path/to/file>`

To exit the terminal:
`exit`

To reset the console if it gets weird:
`reset`

### NAVIGATION ###
To list the contents of the current directory:
`ls`

To change the directory:
`cd </path/to/directory>`

To go up one directory:
`cd ..`

To go to your home directory:
`cd ~`

To find a file with a given name:
`find -iname <file_name>`

### EDITING FILES ###
To open a text file in a commandline editor:
`nano </path/to/file>`

To open a text file in a GUI editor:
`xed </path/to/file>`

### NANO ###
To save the current file:
`ctrl+o`

To exit nano:
`ctrl+x`

To cut current line to clipboard:
`ctrl+k`

To paste line from the previous command:
`ctrl+u`

To use the find/replace functionality:
`ctrl+\`

To search for a specific string:
`ctrl+w`

### EDITING DIRECTORIES ###

To create a new directory:
`mkdir </path/to/dir>`

To remove a directory:
`rm -r </path/to/dir>`

To copy a file:
`cp </path/to/original> </path/to/destination>`

To move a file (can also be used to rename a file):
`mv </path/to/original> </path/to/destination>` # WARNING - include a trailing / to prevent overwriting the destination dir with the file

To delete a file:
`rm </path/to/file>`
