#!/bin/bash

# Setup script for the programming laptops
# 2017-01-07

echo "Begin setup"

## Download and install Slack
cd ~/Downloads
echo "Installing Slack..."
curl -s https://downloads.slack-edge.com/linux_releases/slack-desktop-2.3.4-amd64.deb -o slack.deb
sudo dpkg -i slack.deb

## Download and install packages
echo "Installing Eclipse..."
sudo apt-get -q install git eclipse
echo "Dont forget to set up the JDK and FRC plugins with Eclipse!"

## Configure user account
echo "Creating User Account..."
useradd --create-home programming
passwd programming frcteam4139

## Download and install driverstation
echo "Installing Driverstation"
curl -s https://github.com/FRC-Utilities/QDriverStation/releases/download/v16.08/qdriverstation-16.08-linux.deb -O driver.deb
sudo dpkg -i driver.deb

echo "Setup Complete"
