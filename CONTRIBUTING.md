### Contribution Guide ###

### These rules must be followed for all contributions to this repository ###

- All code must use Allman-style brackets.

`public void main()`

`{`

`}`

- Commit messages must begin with a present tense verb and followed with a short description of what was done.

`Add new page`

`Replace int with double`

- Tabs are used for indentation, spaces for allignment

- Branches are named for what they are changing or adding, in all lower case, split with underscores

`vision_processing`

`drivetrain_bugfixing`

- Method names are camel case

`medthodName`

`thisIsMyMethod`

- Constant names are all caps, split with underscores

`CONSTANT_NAME`

- Variable names are camel case

`variableName`

- Versioning must follow the guidelines at www.semver.org